/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

import java.util.ArrayList;
import model.Matches;
import model.Players;
import model.Users;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Assert;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

/**
 *
 * @author patop
 */
public class powersAndWolfAssigned {
    ArrayList<Players> listP;
    Matches oMatch;
    
    public powersAndWolfAssigned() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
        oMatch = new Matches(1);//Create the match
        oMatch.setfWolfProportion(30);
        oMatch.setfPowerProbability(70);
        listP = new ArrayList<>();
        for (int i = 0; i < 10; i++) {//Create 10 players
            Players oPlayers = new Players(new Users("user"+i));
            oPlayers.setoMatch(oMatch);
            listP.add(oPlayers);//Add a player to the list.
        }
    }
    
    @After
    public void tearDown() {
    }

    public void setWolf(){
        int iWolfs = (int) (oMatch.getfWolfProportion()*listP.size()/100);
        if(iWolfs == 0)//If proportion was 0, 1 player at least must be wolf.
            iWolfs = 1;
        for (Players oPlayer : listP) {//set if a player is wolf or citizen.
            if(iWolfs != 0){
                oPlayer.setIsWolf(true);
                iWolfs--;
            }
        }
    }
    @Test
    public void assigedWolf() {
        setWolf();
        int countPlayer = 0;
        for (Players players : listP) {
            if(players.isIsWolf())
                countPlayer++;
        }
        Assert.assertEquals(countPlayer,listP.size()*oMatch.getfWolfProportion()/100,0.50);
    }
    
    /**
     *
     */
    @Test
    public void assigedPowers(){
        setWolf();
        if(Math.random()<= oMatch.getfPowerProbability()){//Check if powers will be assigned.
            String[] sPowers = new String[]{"Spiritisme","Voyance","Contamination","Insomnie"};
            for (String sPower : sPowers) {//For each power.
                boolean loop = true;
                while(loop){//This loop is for repeat the process until the power is assigned.
                    int iPlayerNummber = (int) (Math.random()*(listP.size()-1));//Obtein the player number.
                    Players oPlayer = listP.get(iPlayerNummber);
                    if(oPlayer.getsPower() != null)//If player already has a power
                        continue;
                    boolean flag = true;
                    switch (sPower){
                        case "Contamination":
                            if(!oPlayer.isIsWolf())//Contamination is only for wolf, if the player is a citizen, do not assign it.
                                flag = false;
                            break;
                        case "Insomnie":
                            if(oPlayer.isIsWolf())//Insomnie is only for citizens, if the player is a wolf, do not assign it.
                                flag = false;
                            break;
                    }
                    if(flag){
                        oPlayer.setsPower(sPower);
                        loop = false;
                    }
                }
            }
        }
        int countPlayer = 0;
        for (Players players : listP) {
            if(players.getsPower() != null)
                countPlayer++;
        }
        Assert.assertEquals(4,countPlayer);
    }
     
}
