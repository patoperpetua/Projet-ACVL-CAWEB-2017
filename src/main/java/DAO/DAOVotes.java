/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package DAO;

import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import javax.sql.DataSource;
import model.Designations;
import model.Matches;
import model.Players;
import model.Users;
import model.Votes;

/**
 *
 * @author Anna Paola
 */
public class DAOVotes extends AbstractDataBaseDAO{
    
    public DAOVotes(DataSource ds) {
        super(ds);
    }
    public ArrayList<Votes> getList(Clauses oClauses){
        ArrayList<Votes> result = new ArrayList<>();
        try (
	     Connection conn = getConn();
	     Statement st = conn.createStatement();
	     ) {
            ResultSet rs;
            if(oClauses == null)
                rs = st.executeQuery("SELECT * FROM Votes");
            else
                rs = st.executeQuery("SELECT * FROM Votes " + oClauses.toString());
            while (rs.next()) {
                Votes oPlayer =
                    new Votes(new Players(new Users(rs.getString("username"))),
                        rs.getBoolean("isPositive"),new Designations(rs.getInt("idDesignations")),
                        rs.getInt("idVotes"));
                result.add(oPlayer);
            }
        } catch (SQLException e) {
            throw new ExceptionDAO("Erreur BD " + e.getMessage(), e);
	}
        return result;
    }
    
    public void insert(Votes oVote){
        try (
	     Connection conn = getConn();
	     PreparedStatement st = conn.prepareStatement
	       ("INSERT INTO Votes (\"isPositive\", \"username\","
                    + "\"idDesignations\",\"idVotes\") VALUES (?, ?, ?"
                       + ", vote_sequence.nextval)");
	     ) {
            st.setBoolean(1, oVote.isIsPositive());
            st.setString(2, oVote.getoPlayer().getoUser().getsUsername());
            st.setInt(3, oVote.getoDesignation().getiID());
            st.executeUpdate();
        } catch (SQLException e) {
            throw new ExceptionDAO("Erreur BD " + e.getMessage(), e);
        }
    }
    
    public Votes get(Clauses oClauses){
        Votes result = null;
        try (
	     Connection conn = getConn();
	     Statement st = conn.createStatement();
	     ) {
            ResultSet rs;
            if(oClauses == null)
                rs = st.executeQuery("SELECT * FROM Votes");
            else
                rs = st.executeQuery("SELECT * FROM Votes " + oClauses.toString());
            while (rs.next()) {
                result =
                    new Votes(new Players(new Users(rs.getString("username"))),
                        rs.getBoolean("isPositive"),new Designations(rs.getInt("idDesignations")),
                        rs.getInt("idVotes"));
            }
        } catch (SQLException e) {
            throw new ExceptionDAO("Erreur BD " + e.getMessage(), e);
	}
        return result;
    }
    
    public void update(Votes oVote){
        try (
	     Connection conn = getConn();
	     PreparedStatement st = conn.prepareStatement
	       ("UPDATE Votes SET \"isPositive\" = ?, \"username\" = ?, \"idDesignations\" "
                    + "= ? WHERE \"idVotes\" = ?");
	     ) {
            st.setBoolean(1, oVote.isIsPositive());
            st.setString(2, oVote.getoPlayer().getoUser().getsUsername());
            st.setInt(3, oVote.getoDesignation().getiID());
            st.setInt(4, oVote.getiID());
            st.executeUpdate();
        } catch (SQLException e) {
            throw new ExceptionDAO("Erreur BD " + e.getMessage(), e);
        }
    }
    
    public void delete(Votes oVote){
        try (
	     Connection conn = getConn();
	     PreparedStatement st = conn.prepareStatement
	       ("DELETE FROM Votes WHERE \"idVotes\" = ?");
	     ) {
            st.setInt(1, oVote.getiID());
            st.executeUpdate();
        } catch (SQLException e) {
            throw new ExceptionDAO("Erreur BD " + e.getMessage(), e);
        }
    }
    
}
