/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package DAO;

import Utils.Dates;
import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import javax.sql.DataSource;
import model.Chats;
import model.Matches;
import model.Messages;
import model.Players;
import model.Users;

/**
 *
 * @author Anna Paola
 */
public class DAOMessages extends AbstractDataBaseDAO{
    
    public DAOMessages(DataSource ds) {
        super(ds);
    }
    public ArrayList<Messages> getList(Clauses oClauses){
        ArrayList<Messages> result = new ArrayList<>();
        try (
	     Connection conn = getConn();
	     Statement st = conn.createStatement();
	     ) {
            ResultSet rs;
            if(oClauses == null)
                rs = st.executeQuery("SELECT * FROM Messages");
            else
                rs = st.executeQuery("SELECT * FROM Messages " + oClauses.toString());
            while (rs.next()) {
                Messages oMatch =
                    new Messages(rs.getString("message"), 
                        new Players(new Users(rs.getString("username"))),
                        rs.getDate("date"),rs.getInt("idMessages"),
                        new Chats(rs.getInt("idChats")));
                result.add(oMatch);
            }
        } catch (SQLException e) {
            throw new ExceptionDAO("Erreur BD " + e.getMessage(), e);
	}
	return result;
    }
    
    public void insert(Messages oMessage){
        try (
	     Connection conn = getConn();
	     PreparedStatement st = conn.prepareStatement
	       ("INSERT INTO Messages (\"message\", \"username\",\"date\", "
                    + "\"idChats\",\"idMessages\") VALUES (?, ?, ?, ?, "
                    + "message_sequence.nextval)");
	     ) {
            st.setString(1, oMessage.getsMessage());
            st.setString(2, oMessage.getoPlayer().getoUser().getsUsername());
            st.setDate(3, new Date(oMessage.getoDate().getTime()));
            st.setInt(4, oMessage.getoChat().getiID());
            st.executeUpdate();
        } catch (SQLException e) {
            throw new ExceptionDAO("Erreur BD " + e.getMessage(), e);
        }
    }
    
    public Messages get(Clauses oClauses){
        Messages result = null;
        try (
	     Connection conn = getConn();
	     Statement st = conn.createStatement();
	     ) {
            ResultSet rs;
            if(oClauses == null)
                rs = st.executeQuery("SELECT * FROM Messages");
            else
                rs = st.executeQuery("SELECT * FROM Messages " + oClauses.toString());
            while (rs.next()) {
                result =
                    new Messages(rs.getString("message"), 
                        new Players(new Users(rs.getString("username"))),
                        rs.getDate("date"),rs.getInt("idMessages"),
                        new Chats(rs.getInt("idChats")));
            }
        } catch (SQLException e) {
            throw new ExceptionDAO("Erreur BD " + e.getMessage(), e);
	}
	return result;
    }
    
    public void update(Messages oMessage){
        try (
	     Connection conn = getConn();
	     PreparedStatement st = conn.prepareStatement
	       ("UPDATE Messages SET \"message\" = ?, \"username\" = ?, \"date\" = ?, "
                    + "\"idChats\" = ? WHERE \"idMessages\" = ?) ");
	     ) {
            st.setString(1, oMessage.getsMessage());
            st.setString(2, oMessage.getoPlayer().getoUser().getsUsername());
            st.setDate(3, new Date(oMessage.getoDate().getTime()));
            st.setInt(4, oMessage.getoChat().getiID());
            st.setInt(5, oMessage.getiID());
            st.executeUpdate();
        } catch (SQLException e) {
            throw new ExceptionDAO("Erreur BD " + e.getMessage(), e);
        }
    }
    
    public void delete(Messages oMessage){
        try (
            Connection conn = getConn();
            PreparedStatement st = conn.prepareStatement
	       ("DELETE FROM Messages WHERE \"idMatches\" = ?");
	     ) {
            st.setInt(1, oMessage.getiID());
            st.executeUpdate();
        } catch (SQLException e) {
            throw new ExceptionDAO("Erreur BD " + e.getMessage(), e);
	}
    }
    
}
