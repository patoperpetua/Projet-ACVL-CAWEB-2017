/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package DAO;


import java.util.ArrayList;
import java.util.HashMap;

/**
 *
 * @author patop
 */
public class Clauses {
    private ArrayList<String> lColumns;
    private ArrayList<String> lValues;
    private ArrayList<String> lCondicion;
    private HashMap lComparator;
    private String sOrder;
    private int iLimit;
    private int iOffset;

    public Clauses() {
        lColumns = new ArrayList<>();
        lValues = new ArrayList<>();
        iLimit = -1;
        iOffset = -1;
    }

    public Clauses(Clauses oClause){
        this.iLimit = oClause.iLimit;
        this.iOffset = oClause.iOffset;
        this.lColumns = oClause.lColumns;
        this.lCondicion = oClause.lCondicion;
        this.lValues = oClause.lValues;
        this.sOrder = oClause.sOrder;
    }
    
    public void add(String sColumn, String sValue){
        addColumn(sColumn);
        addValue(sValue);
    }
    
    public void addWithCondition(String sColumn, String sValue, String sCondicion){
        addColumn(sColumn);
        addValue(sValue);
        addCondicion(sCondicion);
    }
    
    public void add(String sColumn, String sValue, String sComparator, String sCondicion){
        addColumn(sColumn);
        addValue(sValue);
        addCondicion(sCondicion);
        setComparator(sComparator, lValues.size()-1);
    }
    
    public void addWithComparator(String sColumn, String sValue, String sComparator){
        addColumn(sColumn);
        addValue(sValue);
        setComparator(sComparator, lValues.size()-1);
    }
    
    public void addColumn(String sColumn){
        if(lColumns == null)
            lColumns = new ArrayList<>();
        if(sColumn.contains("(")){
            lColumns.add(sColumn);
        }else
            lColumns.add("\""+sColumn+"\"");
    }
    
    public void addValue(String sValue){
        if(lValues == null)
            lValues = new ArrayList<>();
        lValues.add(sValue);
    }
    
    public void addCondicion(String sCondicion){
        if(lCondicion == null)
            lCondicion = new ArrayList<>();
        lCondicion.add(sCondicion);
    }
    
    public ArrayList<String> getlColumns() {
        return lColumns;
    }

    public void setlColumns(ArrayList<String> lColumns) {
        this.lColumns = lColumns;
    }

    public ArrayList<String> getlValues() {
        return lValues;
    }

    public void setlValues(ArrayList<String> lValues) {
        this.lValues = lValues;
    }

    public String getsOrder() {
        return sOrder;
    }

    public void setsOrder(String sOrder) {
        this.sOrder = "\""+ sOrder +"\"" ;
    }

    public int getiLimit() {
        return iLimit;
    }

    public void setiLimit(int iLimit) {
        this.iLimit = iLimit;
    }

    public int getiOffset() {
        return iOffset;
    }

    public void setiOffset(int iOffset) {
        this.iOffset = iOffset;
    }

    public String getValue(String sColumn){
        int iPos = lColumns.indexOf(sColumn);
        if(iPos > 0 && iPos < lColumns.size())
            return lValues.get(iPos);
        return null;
    }
    
    public void setComparator(String sComparator, int iPos){
        if(lComparator == null)
            lComparator = new HashMap();
        lComparator.put(iPos, sComparator);
    }
    
    @Override
    public String toString() {
        String sStatement = "";
        if(lValues != null && !lValues.isEmpty()){
            if(sStatement.isEmpty())
                sStatement += " WHERE ";
            else if(lCondicion == null)
                sStatement += " AND ";
            else
                sStatement+= lCondicion.get(0);
            sStatement+= agregarContenido(lColumns.get(0),lValues.get(0),0);
            for (int i = 1; i < lColumns.size(); i++) {
                if(lCondicion == null)
                    sStatement+= " AND ";
                else
                    sStatement+= " "+lCondicion.get(i)+" ";
                sStatement+=agregarContenido(lColumns.get(i),lValues.get(i),i);
            }
        }
        if(sOrder != null && !sOrder.isEmpty())
            sStatement+= " ORDER BY " + sOrder +" ";
        if(iOffset != -1)
            sStatement+= " OFFSET "+iOffset + " ";
        if(iLimit != -1)
            sStatement+= " LIMIT "+iLimit + " ";
        return sStatement;
    }
    
    private String agregarContenido(String sSelec, String sData, int iPos){
        if(lComparator == null){
            return agregarContenido(sSelec, sData);
        }else{
            if(lComparator.get(iPos) == null)
                return agregarContenido(sSelec, sData);
            else{
                return agregarContenido(sSelec+lComparator.get(iPos), sData);
            }
        }
    }
    
    private String agregarContenido(String sSelec, String sData){
        if(sSelec.contains("<") || sSelec.contains(">")|| sSelec.contains("=")){
            return sSelec + " "+sData+"";
        }
        else if (sData.contains("last_insert_id()"))
            return sSelec + " = "+sData+" ";
        else if (sData.contains("NULL") || sData.contains("null"))
            return  sSelec+" IS NULL ";
        else
            return sSelec + " like '"+sData+"' ";
        
    }
    
    public void clear(){
        lColumns.clear();
        lValues.clear();
        if(lCondicion != null)
            lCondicion.clear();
        if(lComparator != null)
            lComparator.clear();
    }
}
