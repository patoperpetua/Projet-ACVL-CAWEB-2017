/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

import java.util.ArrayList;
import java.util.Date;

/**
 *
 * @author Anna Paola
 */
public class Chats {
    private Date oDate;
    private boolean isForWolf;
    private Matches oMatch;
    private int iID;
    private ArrayList<Messages> listMessages;
    public int getiID() {
        return iID;
    }

    public void setiID(int iID) {
        this.iID = iID;
    }
    public Chats() {
    }

    public Chats(int iID) {
        this.iID = iID;
    }

    public Chats(Date oDate, boolean isForWolf, Matches oMatch, int iID) {
        this.oDate = oDate;
        this.isForWolf = isForWolf;
        this.oMatch = oMatch;
        this.iID = iID;
    }


    public Matches getoMatch() {
        return oMatch;
    }

    public void setoMatch(Matches oMatch) {
        this.oMatch = oMatch;
    }
    
    public boolean isOpen(){
        return true;
    }

    public Date getoDate() {
        return oDate;
    }

    public void setoDate(Date oDate) {
        this.oDate = oDate;
    }

    public boolean isIsForWolf() {
        return isForWolf;
    }

    public void setIsForWolf(boolean isForWolf) {
        this.isForWolf = isForWolf;
    }

    public ArrayList<Messages> getListMessages() {
        return listMessages;
    }

    public void setListMessages(ArrayList<Messages> listMessages) {
        this.listMessages = listMessages;
    }
}

