/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

import java.util.Date;

/**
 *
 * @author Anna Paola
 */
public class Messages {
    private String sMessage;
    private Players oPlayer;
    private Date oDate;
    private int iID;
    private Chats oChat;

    public Messages() {
    }

    public Chats getoChat() {
        return oChat;
    }

    public void setoChat(Chats oChat) {
        this.oChat = oChat;
    }

    public Messages(String sMessage, Players oPlayer, Date oDate, int iID, Chats oChat) {
        this.sMessage = sMessage;
        this.oPlayer = oPlayer;
        this.oDate = oDate;
        this.iID = iID;
        this.oChat = oChat;
    }
    

    public int getiID() {
        return iID;
    }

    public void setiID(int iID) {
        this.iID = iID;
    }

    public String getsMessage() {
        return sMessage;
    }

    public void setsMessage(String sMessage) {
        this.sMessage = sMessage;
    }

    public Players getoPlayer() {
        return oPlayer;
    }

    public void setoPlayer(Players oPlayer) {
        this.oPlayer = oPlayer;
    }

    public Date getoDate() {
        return oDate;
    }

    public void setoDate(Date oDate) {
        this.oDate = oDate;
    }
}
