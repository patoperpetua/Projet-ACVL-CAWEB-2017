/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

import java.util.Objects;

/**
 *
 * @author Anna Paola
 */
public class Players {
    private Users oUser;
    private boolean isWolf;
    private String sPower;
    private boolean isAlived;
    private Matches oMatch;
    public Players(Users oUser) {
        this.oUser = oUser;
        this.isAlived = true;
    }

    public Players(Users oUser, boolean isWolf, String sPower, boolean isAlived, Matches oMatch) {
        this.oUser = oUser;
        this.isWolf = isWolf;
        this.sPower = sPower;
        this.isAlived = isAlived;
        this.oMatch = oMatch;
    }

    public Matches getoMatch() {
        return oMatch;
    }

    public void setoMatch(Matches oMatch) {
        this.oMatch = oMatch;
    }

    public Users getoUser() {
        return oUser;
    }

    public void setoUser(Users oUser) {
        this.oUser = oUser;
    }

    public boolean isIsWolf() {
        return isWolf;
    }

    public void setIsWolf(boolean isWolf) {
        this.isWolf = isWolf;
    }

    public String getsPower() {
        return sPower;
    }

    public void setsPower(String sPower) {
        this.sPower = sPower;
    }

    public boolean isIsAlived() {
        return isAlived;
    }

    public void setIsAlived(boolean isAlived) {
        this.isAlived = isAlived;
    }

    @Override
    public int hashCode() {
        int hash = 5;
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Players other = (Players) obj;
        if (!Objects.equals(this.oUser, other.oUser)) {
            return false;
        }
        return true;
    }

}
