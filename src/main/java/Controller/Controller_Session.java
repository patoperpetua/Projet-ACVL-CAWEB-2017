/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Controller;

import DAO.Clauses;
import DAO.DAOUsers;
import java.io.IOException;
import java.util.ArrayList;
import javax.annotation.Resource;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.sql.DataSource;
import model.Users;

/**
 *
 * @author perpetup
 */
@WebServlet(name = "Controller_Session", urlPatterns = {"/controller_Session"})
public class Controller_Session extends HttpServlet {
    @Resource(name = "jdbc/Projet-ACVL-CAWEB-2017")
    private DataSource ds;
    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        DAOUsers oDAO;
        Clauses oClause;
        String sAction = request.getParameter("action");
        String sView = request.getParameter("view");
        if(sAction == null ){
            if(sView == null)
            {
                HttpSession session = request.getSession(false);
                if(session == null || session.getAttribute("username") != null){
                    request.getRequestDispatcher("/Users/logIn.jsp").forward(request, response);
                }else
                    request.getRequestDispatcher("Game/gameSelection.jsp").forward(request, response);
            }
            else
            {
                switch(sView){
                    case "controller_Session":
                        DAOUsers oDAOUsers = new DAOUsers(ds);
                        ArrayList<Users> result = oDAOUsers.getList(null);
                        request.setAttribute("list", result);
                    break;
                    case "rules":
                    
                    break;
                    case "about":
                    
                    break;
                }
                request.getRequestDispatcher("/Users/"+sView+".jsp").forward(request, response);
            }            
        }
        else{
            switch(sAction){
                case "add":
                    oDAO = new DAOUsers(ds);
                    Users oUser = getDataFromRequest(request);
                    if(oDAO.insert(oUser)){
                        HttpSession session = request.getSession(false);
                        if(session != null)
                            session.invalidate();
                        session = request.getSession();
                        session.setAttribute("username", oUser.getsUsername());
                        response.sendRedirect("Controller_Match");
                    }
                    break;
                case "get":
                    oDAO = new DAOUsers(ds);
                    oClause = getClauseFromRequest(request);
                    oUser = oDAO.get(oClause);
                    break;
                case "getAll":
                    oDAO = new DAOUsers(ds);
                    oClause = getClauseFromRequest(request);
                    ArrayList<Users> list = oDAO.getList(oClause);
                    break;
                case "erase":
                    oDAO = new DAOUsers(ds);
                    oDAO.delete(getDataFromRequest(request));
                    break;
                case "update":
                    oDAO = new DAOUsers(ds);
                    oDAO.update(getDataFromRequest(request));
                    break;
                case "login":
                    oDAO = new DAOUsers(ds);
                    oClause = getClauseFromRequest(request);
                    oUser = oDAO.get(oClause);
                    if(oUser != null){
                        HttpSession session = request.getSession(false);
                        if(session.getAttribute("username") != null)
                            session.invalidate();
                        session = request.getSession();
                        session.setAttribute("username", oUser.getsUsername());
                        response.sendRedirect("Controller_Match");
                    }else{
                        request.setAttribute("Error", "User: "+oClause.getValue("username")+". Sing In!");
                        request.getRequestDispatcher("Users/controller_Session.jsp").forward(request, response);
                    }
                    break;
                case "logOut":
                    HttpSession session = request.getSession(false);
                        if(session != null){
                            session.removeAttribute("username");
                            session.invalidate();
                            request.logout();
                        }
                    request.getRequestDispatcher("Users/logIn.jsp").forward(request, response);
                    break;
                default:
                    invalidParameters(request, response);
                    break;
            }
        }    
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

    private Users getDataFromRequest(HttpServletRequest request) {
        return new Users(request.getParameter("username"), request.getParameter("pwd1"));
    }

    private Clauses getClauseFromRequest(HttpServletRequest request) {
        Clauses oClause = null;
        String sUsername  = request.getParameter("username");
        if(sUsername != null){
            oClause = new Clauses();
            oClause.add("username", sUsername);
            String sPass  = request.getParameter("pwd1");
            if(sPass != null)
                oClause.add("pass", sPass);
        }
        return oClause;
    }

    private void invalidParameters(HttpServletRequest request,
            HttpServletResponse response) throws ServletException, IOException {
        request.getRequestDispatcher("/WEB-INF/ErrorControler.jsp").forward(request, response);        
    }
}