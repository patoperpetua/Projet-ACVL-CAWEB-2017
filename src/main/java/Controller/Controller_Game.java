/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Controller;

import Utils.Dates;
import DAO.Clauses;
import DAO.DAOChats;
import DAO.DAODesignations;
import DAO.DAOMatches;
import DAO.DAOMessages;
import DAO.DAOPlayers;
import DAO.DAOVotes;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import javax.annotation.Resource;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.sql.DataSource;
import model.Chats;
import model.Designations;
import model.Matches;
import model.Messages;
import model.Players;
import model.Users;
import model.Votes;

/**
 *
 * @author perpetup
 */
@WebServlet(name = "Controller_Game", urlPatterns = {"/Controller_Game"})
public class Controller_Game extends HttpServlet {
    @Resource(name = "jdbc/Projet-ACVL-CAWEB-2017")
    private DataSource ds;
    private Matches oMatch;
    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        String sMatch = (String) request.getParameter("idMatches");
        if(sMatch == null)
            invalidParameters(request, response);
        else{
            String sAction = request.getParameter("action");
            String sView = request.getParameter("view");
            DAOMatches oDAOMatches = new DAOMatches(ds);
            Clauses oClause = new Clauses();
            oClause.add("idMatches", sMatch);
            oMatch = oDAOMatches.get(oClause);
            System.out.println(Dates.sd.format(oMatch.getFinalDayHour()));
            if(oMatch == null)
                invalidParameters(request, response);
            else{
                HttpSession oSession = request.getSession();
                if(oSession == null)
                    invalidParameters(request, response);
                else{
                    if(sAction == null ){//If no action is requested
                        if(sView == null)//If no view is requested.
                        { 
                            putDataIntoRequestAndSendIt(request,response);
                        }else{
                            switch(sView){
                                case "chat":
                                    String sChat = request.getParameter("idChats");
                                    if(sChat == null)
                                        invalidParameters(request, response);
                                    else{
                                        DAOChats oDAOChats = new DAOChats(ds);
                                        if(oClause == null)
                                            oClause = new Clauses();
                                        else
                                            oClause.clear();
                                        oClause.add("idChats", sChat);
                                        Chats oChat = oDAOChats.get(oClause);
                                        if(oChat == null)
                                            invalidParameters(request, response);
                                        else{
                                            request.setAttribute("thisChat", oChat);
                                            request.getRequestDispatcher("/Game/"+sView+".jsp").forward(request, response);
                                        }
                                    }
                                    break;
                                case "finishedMatch":
                                    matchFinishedView(request, response);
                                    break;
                                default:
                                    invalidParameters(request, response);
                                    break;
                            }
                        }
                    }else{
                        switch(sAction){
                            case "addDesignation":
                                Players oPlayer = (Players) oSession.getAttribute("thisPlayer");
                                String sAccused = request.getParameter("Accused");
                                if(oPlayer == null || sAccused == null)
                                    invalidParameters(request, response);
                                else{
                                    Designations oDesignation = new Designations();
                                    oDesignation.setoDate(new Date());
                                    oDesignation.setoMatch(oMatch);
                                    oDesignation.setoPlayerAcussed(new Players(new Users(sAccused)));
                                    oDesignation.setoPlayerAcusser(oPlayer);
                                    oDesignation.setForWolf(oPlayer.isIsWolf());
                                    DAODesignations oDAODesignations = new DAODesignations(ds);
                                    oDAODesignations.insert(oDesignation);
                                    putDataIntoRequestAndSendIt(request, response);
                                }
                                break;
                            case "Vote":
                                oPlayer = (Players) oSession.getAttribute("thisPlayer");
                                String sPositive = request.getParameter("isPositive");
                                String sidDesignations = request.getParameter("idDesignations");
                                if(oPlayer == null || sPositive == null || sidDesignations == null)
                                    invalidParameters(request, response);
                                else{
                                    Votes oVote = new Votes();
                                    if(sPositive.equals("yes"))
                                        oVote.setIsPositive(true);
                                    else
                                        oVote.setIsPositive(false);
                                    oVote.setoDesignation(new Designations(Integer.parseInt(sidDesignations)));
                                    oVote.setoPlayer(oPlayer);
                                    DAOVotes oDAOVotes = new DAOVotes(ds);
                                    oDAOVotes.insert(oVote);
                                    putDataIntoRequestAndSendIt(request, response);
                                }
                            break;
                            case "addMessage":
                                oPlayer = (Players) oSession.getAttribute("thisPlayer");
                                String sMessage = request.getParameter("message");
                                Chats oChat = (Chats) oSession.getAttribute("thisChat");
                                if(sMessage == null || oPlayer == null || oChat == null
                                    || sMessage.isEmpty())
                                    invalidParameters(request, response);
                                else{
                                    Messages oMessage = new Messages();
                                    oMessage.setoChat(oChat);
                                    oMessage.setoDate(new Date());
                                    oMessage.setoPlayer(oPlayer);
                                    oMessage.setsMessage(sMessage);
                                    DAOMessages oDAOMessages = new DAOMessages(ds);
                                    oDAOMessages.insert(oMessage);
                                    putDataIntoRequestAndSendIt(request, response);
                                }
                                break;
                            default:
                                invalidParameters(request, response);
                                break;
                        }
                    }
                }
                
            }
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>
    
    private void invalidParameters(HttpServletRequest request,
            HttpServletResponse response) throws ServletException, IOException {
        request.getRequestDispatcher("/WEB-INF/ErrorControler.jsp").forward(request, response);        
    }

    private void putDataIntoRequestAndSendIt(HttpServletRequest request,HttpServletResponse response) throws ServletException, IOException {
        Clauses oClause;
        HttpSession oSession = request.getSession();
        DAOPlayers oDAOPlayers = new DAOPlayers(ds);
        oSession.setAttribute("thisMatch", oMatch);
        oClause = new Clauses();
        oClause.add("idMatches", oMatch.getiID()+"");
        ArrayList<Players> listPlayers = oDAOPlayers.getList(oClause);
        if(checkMatchIsFinished()){
            oMatch.setFinished(true);
            DAOMatches oDAOMatches = new DAOMatches(ds);
            oDAOMatches.update(oMatch);
            matchFinishedView(request,response);
        }else{
            oClause.add("username", (String) oSession.getAttribute("username"));
            Players oPlayer = oDAOPlayers.get(oClause);
            DAOChats oDAOChats = new DAOChats(ds);
            oClause.clear();
            oClause.add("idMatches", oMatch.getiID()+"");
            oClause.addWithComparator("TO_DATE(\"date\")", "TO_DATE('"+Dates.sdDays.format(new Date())+"','"+Dates.sdDays.toPattern()+"')","=");
            Chats oChat = null;
            if(oPlayer.isIsWolf()){    
                if(oPlayer.isIsAlived()){//If the player is a wolf, chat is always available, let's find the current chat available
                    oChat = obteinChatForWolf(oClause,oDAOChats);
                }
            }else//If the player is a citizen, only chat for citizen can be accessed 
                oClause.add("isForWolf","0");
            if(oChat == null)
                oChat = oDAOChats.get(oClause);
            if(oChat == null){
                oChat = new Chats();
                oChat.setIsForWolf(oPlayer.isIsWolf());
                oChat.setoDate(new Date());
                oChat.setoMatch(oMatch);
                oDAOChats.insert(oChat);
                oChat.setIsForWolf(!oChat.isIsForWolf());
                oDAOChats.insert(oChat);
                if(oPlayer.isIsAlived()){//If the player is a wolf, chat is always available, let's find the current chat available
                    if(oPlayer.isIsWolf())
                        oChat = obteinChatForWolf(oClause,oDAOChats);
                    else
                        oChat = oDAOChats.get(oClause);
                }
            }
            ArrayList<Designations> listDesignations = checkDesignations(listPlayers,oPlayer,oChat.isIsForWolf());
            oSession.setAttribute("thisPlayer", oPlayer);
            oClause.clear();
            oClause.add("idMatches", oMatch.getiID()+"");
            if(!oPlayer.isIsWolf())
                oClause.add("isForWolf","0");
            oClause.addWithComparator("idChats", oChat.getiID()+"","!=");
            ArrayList<Chats> listChats = oDAOChats.getList(oClause);
            oSession.setAttribute("thisChat", oChat);
            request.setAttribute("listPlayers", listPlayers);
            request.setAttribute("listDesignations", listDesignations);
            request.setAttribute("listChats", listChats);
            request.getRequestDispatcher("Game/playingGame.jsp").forward(request, response);
        }
    }

    private Chats obteinChatForWolf(Clauses oClause, DAOChats oDAOChats) {
        ArrayList<Chats> listAux = oDAOChats.getList(oClause);
        Date oDate = new Date();
        Chats oChat = null;
        for (Chats oCha : listAux) {
            Date oChatStart;
            Date oChatFinal;
            Calendar c = Calendar.getInstance();
            int iHoursStart;
            int iMinutesStart;
            int iHoursFinal;
            int iMinutesFinal;
            if(oCha.isIsForWolf()){
                c.setTime(oMatch.getFinalDayHour());
                iHoursStart = c.get(Calendar.HOUR_OF_DAY);
                iMinutesStart = c.get(Calendar.MINUTE);
                c.setTime(oMatch.getoHourStart());
                iHoursFinal = c.get(Calendar.HOUR_OF_DAY);
                iMinutesFinal = c.get(Calendar.MINUTE);
            }else{
                c.setTime(oMatch.getoHourStart());
                iHoursStart = c.get(Calendar.HOUR_OF_DAY);
                iMinutesStart = c.get(Calendar.MINUTE);
                c.setTime(oMatch.getFinalDayHour());
                iHoursFinal = c.get(Calendar.HOUR_OF_DAY);
                iMinutesFinal = c.get(Calendar.MINUTE);
            }
            c.setTime(new Date());
            c.set(Calendar.HOUR_OF_DAY, iHoursStart);
            c.set(Calendar.MINUTE, iMinutesStart);
            oChatStart = c.getTime();
            c.setTime(new Date());
            c.set(Calendar.HOUR_OF_DAY, iHoursFinal);
            c.set(Calendar.MINUTE, iMinutesFinal);
            if(iHoursFinal == 0)
                c.add(Calendar.DAY_OF_YEAR, 1);
            oChatFinal = c.getTime();
            if(oChatStart.getTime() < oDate.getTime() && oChatFinal.getTime() > oDate.getTime()){
                oChat = oCha;
                break;
            }
        }
        return oChat;
    }

    private ArrayList<Designations> checkDesignations(ArrayList<Players> listPlayers,
            Players oPlayer, boolean isForWolf) {
        DAOPlayers oDAOPlayers = new DAOPlayers(ds);
        Clauses oClause = new Clauses();
        oClause.add("idMatches", oMatch.getiID()+"");
        if(isForWolf)
            oClause.add("isForWolf", "1");
        else
            oClause.add("isForWolf", "0");
        oClause.addWithComparator("TO_DATE(\"date\")", "TO_DATE('"+Dates.sdDays.format(new Date())+"','"+Dates.sdDays.toPattern()+"')","=");
        DAODesignations oDAODesignations = new DAODesignations(ds);
        ArrayList<Designations> listDesignations= oDAODesignations.getList(oClause);
        int iLivedPlayers = 0;
        int iLivedWolfs = 0;
        for (Players oPla : listPlayers) {
            if(oPla.isIsAlived()){
                iLivedPlayers++;
                if(oPla.isIsWolf())
                    iLivedWolfs++;
            }
        }
        for (Designations oDesignation : listDesignations) {//Check if any designation is completed.
            int iLimit;
            if(oDesignation.isForWolf())
                iLimit = iLivedWolfs;
            else
                iLimit = iLivedPlayers;
            if(oDesignation.howManyPositivesVotes() > iLimit / 2){//
                Players oAccused = listPlayers.get(listPlayers.indexOf(oDesignation.getoPlayerAcussed()));
                if(oAccused != null){
                    oAccused.setIsAlived(false);
                    if(oAccused.isIsAlived()){
                        oDAOPlayers.update(oAccused);
                        if(oAccused.equals(oPlayer))
                            oPlayer.setIsAlived(false);
                    }
                    oDesignation.setCompleted(true);
                    oDesignation.setPositive(true);
                }
            }else if(oDesignation.howManyNegativesVotes() > iLimit / 2){
                oDesignation.setCompleted(true);
                oDesignation.setPositive(false);
            }
        }
        return listDesignations;
    }

    private boolean checkMatchIsFinished() {
        Clauses oClause = new Clauses();
        oClause.add("idMatches", oMatch.getiID()+"");
        oClause.add("isWolf", "1");
        oClause.add("isAlive", "1");
        DAOPlayers oDAOPlayers = new DAOPlayers(ds);
        ArrayList<Players> list = oDAOPlayers.getList(oClause);
        if(list.isEmpty())//If there is not any wolf alive. Match is finished.
            return true;
        else{
            oClause.clear();
            oClause.add("idMatches", oMatch.getiID()+"");
            oClause.add("isWolf", "");
            oClause.add("isAlive", "1");
            list = oDAOPlayers.getList(oClause);
            if(list.isEmpty())//If there is not any citizen alive. Match is finished.
                return true;
            return false;
        }
    }

    private void matchFinishedView(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        if(request.getSession().getAttribute("thisMatch") == null)//If the match was not added, add it.
            request.getSession().setAttribute("thisMatch", oMatch);
        Clauses oClause = new Clauses();
        oClause.add("idMatches", oMatch.getiID()+"");
        DAOPlayers oDAOPlayers = new DAOPlayers(ds);
        ArrayList<Players> listPlayers = oDAOPlayers.getList(oClause);
        oClause.clear();
        oClause.add("idMatches", oMatch.getiID()+"");
        DAOChats oDAOChats = new DAOChats(ds);
        ArrayList<Chats> listChats = oDAOChats.getList(oClause);
        request.setAttribute("listPlayers", listPlayers);
        request.setAttribute("listChats", listChats);
        request.getRequestDispatcher("Game/gameFinish.jsp").forward(request, response);
    }
}
