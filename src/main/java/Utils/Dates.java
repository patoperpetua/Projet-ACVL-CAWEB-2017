/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Utils;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

/**
 *
 * @author patop
 */
public class Dates {
    
    public static SimpleDateFormat sdDays = new SimpleDateFormat("yyyy-MM-dd");
    public static SimpleDateFormat sdHoursAndMinutes = new SimpleDateFormat("HH:mm");
    public static SimpleDateFormat sdHours= new SimpleDateFormat("HH");
    public static SimpleDateFormat sdMinutes = new SimpleDateFormat("mm");
    public static SimpleDateFormat sd = new SimpleDateFormat("yyyy/MM/dd HH:mm");
    public static Date substractDate(Date oDate, int iAmount, int iType){
        if(oDate == null)
            oDate = new Date();
        Calendar c = Calendar.getInstance(); 
        c.setTime(oDate); 
        c.add(iType,iAmount);
        return c.getTime();
    }
    public static String substractDate(int iAmount, int iType,Date oDate){
        return sdDays.format(substractDate(oDate, iAmount, iType));
    }
    
    public static String substractDate(int iAmount, int iType){
        return substractDate(iAmount, iType, null);
    }
}
