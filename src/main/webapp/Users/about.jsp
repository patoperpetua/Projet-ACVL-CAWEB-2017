<%-- 
    Document   : about
    Created on : 09/04/2017, 17:06:30
    Author     : patop
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ page errorPage="../WEB-INF/error.jsp" %>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <link rel="stylesheet" type="text/css" href="CSS/styles.css">
        <title>WereWolf About</title>
    </head>
    <body>
        <jsp:include page="../header.jsp"/>
        <div>
            <h1>Authors!</h1>
            <h2>ANTONINI Anna</h2>
            <h2>FITE Juan Manuel</h2>
            <h2>PERPETUA Patricio</h2>
            <h2>VIÑAS VISCARDI Dardo</h2>
        </div>
        <jsp:include page="../footer.jsp"/>
    </body>
</html>
