<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ page errorPage="../WEB-INF/error.jsp" %>
<html>
  <head>
      <link rel="icon" href="Resourses/Images/favicon.png">
      <link rel="stylesheet" type="text/css" href="CSS/styles.css">
      <title>Welcome</title>
      <script type="text/javascript" src="JS/script.js"></script> 
  </head>
  <body>
    <div class="flex-container">
        <jsp:include page="../header.jsp"/>
        <article class="article">
            <h2>WELCOME</h2>
            <form method="POST" action="controller_Session?action=login"  onSubmit="return valider()">
                <p>Username: <input type="text" id="Username" name="username" ><br/>
                <p>Password: <input type="password" id="pwd1" name="pwd1" ><br/>
                <p><input type="submit" value="Log in"/><br/>
            </form>
            <a href="controller_Session?view=controller_Session" >Join now</a>
        </article>
    
    </div>
    <jsp:include page="../footer.jsp"/>
  </body>
</html>

