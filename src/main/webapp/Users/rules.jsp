<%-- 
    Document   : rules
    Created on : 09/04/2017, 17:06:22
    Author     : patop
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ page errorPage="../WEB-INF/error.jsp" %>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <link rel="stylesheet" type="text/css" href="CSS/styles.css">
        <title>WereWolf - Rules</title>
    </head>
    <body>
        <jsp:include page="../header.jsp"/>
        <div>
            <h1 class="rules">
                Each player can make a designation and only will pass the most voted </br>
                During the day the chat can be used by all players. During the night only the Werewolves </br>
                At the end of the day, if a designation result positive, that person accused will be killed.</br>
                For a decision to be approved must have the majority of votes </br>
                Dead players can view archived chat </br>
                Specials Powers: </br>
                -Contamination(only Werewolves): allows the player to transform a human into a werewolf every night</br>
                -Insomnia(only humans): allows the player to attend discussions of the werewolf's den (but not to participate by posing as a werewolf)</br>
                -Clairvoyance: allows the player to know the role and powers of a player of his choice each night</br>
                -Spiritism: Allows the player to speak with an eliminated player of his choice each night. It is a discussion room similar to the village square but with only two participants</br></br>
                
                For more information consult the user manual
            </h1>
        </div>
        <jsp:include page="../footer.jsp"/>
    </body>
</html>
