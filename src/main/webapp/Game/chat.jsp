<%-- 
    Document   : chats
    Created on : 09/04/2017, 14:25:21
    Author     : patop
--%>

<%@page import="model.Messages"%>
<%@page import="model.Players"%>
<%@page import="Utils.Dates"%>
<%@page import="java.util.Date"%>
<%@page import="model.Chats"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ page errorPage="../WEB-INF/error.jsp" %>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <link rel="stylesheet" type="text/css" href="CSS/styles.css">
        <title>Were Wolf - Archived Chat</title>
    </head>
    <body>
        <jsp:include page="../header.jsp"/>
        <article class="article">
            <div class="column-center">
                <%
                    Chats oThisChat = (Chats) request.getAttribute("thisChat");
                    Players oThisPlayer = (Players) request.getSession().getAttribute("thisPlayer");
                    if(oThisChat != null){
                        out.print("<h1>"+"Archived Chat, date: "+ Dates.sdDays.format(oThisChat.getoDate()) +"</h1>");
                        out.print("<h2>"+"Match N°: "+ oThisChat.getiID() +"</h2>");
                        if(oThisChat.isIsForWolf())
                            out.print("<p>"+"Wolf Chat"+"</p>");
                        else
                            out.print("<p>"+"Citizen Chat"+"</p>");
                        if (oThisChat != null && oThisChat.getListMessages()
                                != null && !oThisChat.getListMessages().isEmpty()) {
                            out.print("<ul>");
                            for (Messages oMessage : oThisChat.getListMessages()) {
                                out.print("<li>");
                                out.print(Dates.sd.format(oMessage.getoDate()) + " ");
                                out.print(oMessage.getoPlayer().getoUser().getsUsername() + ": ");
                                out.print(oMessage.getsMessage());
                                out.print("</li>");
                            }
                            out.print("</ul>");
                        }else
                            out.print("<p>"+"There is no messages in this chat"+"</p>");
                    }
                %>
            </div>
        </article>
        <jsp:include page="../footer.jsp"/>
    </body>
</html>
