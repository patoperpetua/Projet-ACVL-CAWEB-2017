<%-- 
    Document   : gameSelection
    Created on : 29-mar-2017, 18:40:25
    Author     : Anna
--%>
<%@page import="model.Players"%>
<%@page import="Utils.Dates"%>
<%@page import="model.Matches"%>
<%@page import="java.util.ArrayList"%>
<%@page import="java.awt.peer.ListPeer"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ page errorPage="../WEB-INF/error.jsp" %>
<!DOCTYPE html>
<html>
    <head>
        <link rel="icon" href="Resourses/Images/favicon.png">
        <script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>

        <script type="text/javascript" src="JS/script.js"></script>
        <link rel="stylesheet" type="text/css" href="CSS/styles.css">
        <title>Welcome</title>

        <script type="text/javascript" src="JS/jquery.timepicker.js"></script>
        <link rel="stylesheet" type="text/css" href="CSS/jquery.timepicker.css" />
    </head>
    <body>
        <div class="flex-container">
            <jsp:include page="../header.jsp"/>
            <article class="article">
                <h2>...</h2>
                <%
                    Matches oMatch = (Matches) request.getAttribute("match");
                    int iMissingPlayers = Integer.parseInt((String) request.getAttribute("missingPlayers"));
                    out.print("<H2>" + "Match N°" + oMatch.getiID() + "</H2>");
                    out.print("<p>" + "Waiting for start hour:" + Dates.sdHours.format(oMatch.getoHourStart()) + "<p>");
                    out.print("<p>" + "Players can be between " + oMatch.getiNumPlayersMin() + " and " + oMatch.getiNumPlayersMax() + "<p>");
                    out.print("<p>" + iMissingPlayers + " players are missing." + "<p>");
                    ArrayList<Players> listPlayers = (ArrayList<Players>) request.getAttribute("list");
                    out.print("<TABLE>");
                    out.print("<TH>" + "Username" + "</TH>");
                    for (Players oPlayer : listPlayers) {
                        out.print("<TR>");
                        out.print("<TD>" + oPlayer.getoUser().getsUsername() + "</TD>");
                        out.print("</TR>");
                    }
                    out.print("</TABLE>");
                    out.print("<a href=\"Controller_Match?action=leave&idMatches=" + oMatch.getiID() + "\">Leave Match!" + "</a>");
                %>
                <a href="controller_Session?action=logOut">Log Out!</a>
            </article>
        </div>
        <jsp:include page="../footer.jsp"/>
    </body>
</html>
