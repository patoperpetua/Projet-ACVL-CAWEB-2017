<%-- 
    Document   : gameSelection
    Created on : 29-mar-2017, 18:40:25
    Author     : Ariel
--%>
<%@page import="Utils.Dates"%>
<%@page import="model.Matches"%>
<%@page import="java.util.ArrayList"%>
<%@page import="java.awt.peer.ListPeer"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ page errorPage="../WEB-INF/error.jsp" %>
<!DOCTYPE html>
<html>
    <head>
        <link rel="icon" href="Resourses/Images/favicon.png">
        <script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
        
        <script type="text/javascript" src="JS/script.js"></script>
        <link rel="stylesheet" type="text/css" href="CSS/styles.css">
        <title>WereWolf - Match Selection</title>
        
        <script type="text/javascript" src="JS/jquery.timepicker.js"></script>
        <link rel="stylesheet" type="text/css" href="CSS/jquery.timepicker.css" />
    </head>
    <body>
    <div class="flex-container">
        <jsp:include page="../header.jsp"/>
        <article class="article">
            <h2>Matches List</h2>
            <%
                 ArrayList<Matches> listNotFinish=(ArrayList<Matches>) request.getAttribute("listNotFinish");
                 if(listNotFinish != null && listNotFinish.isEmpty())
                 
                 {
                     out.print("<h1> There is not matches availables! ");
                 }
                 else
                 {
                    out.print("<TABLE>");
                    out.print("<TH>"+"Match N°"+"</TH>");
                    out.print("<TH>"+"Date"+"</TH>");
                    out.print("<TH>"+"Start Hour Day"+"</TH>");
                    out.print("<TH>"+"Final Hour Day"+"</TH>");
                    out.print("<TH>"+"Minimum Players"+"</TH>");
                    out.print("<TH>"+"Maximum Players"+"</TH>");
                    out.print("<TH>"+"Action"+"</TH>");
                    for(Matches oMatch : listNotFinish){
                        out.print("<TR>");
                        out.print("<TD>"+oMatch.getiID()+"</TD>");
                        out.print("<TD>"+Dates.sdDays.format(oMatch.getoDate())+"</TD>");
                        out.print("<TD>"+Dates.sdHoursAndMinutes.format(oMatch.getoHourStart())+"</TD>");
                        out.print("<TD>"+Dates.sdHoursAndMinutes.format(oMatch.getFinalDayHour())+"</TD>");
                        out.print("<TD>"+oMatch.getiNumPlayersMin()+"</TD>");
                        out.print("<TD>"+oMatch.getiNumPlayersMax()+"</TD>");
                        out.print("<TD>"+"<a href=\"Controller_Match?action=join&idMatches="+oMatch.getiID()+"\">Join!</a>"+"</TD>");
                        out.print("</TR>");
                    }
                     out.print("</TABLE>");
                 }
            %>
            <br><br><a href="Controller_Match?view=configMatch" class="button">Create Match</a>
            <h2>Finished Matches</h2>
            <%
                 ArrayList<Matches> listFinish=(ArrayList<Matches>) request.getAttribute("listFinish");
                 if(listFinish != null && listFinish.isEmpty())
                 
                 {
                     out.print("<h1> There is not matches availables! ");
                 }
                 else
                 {
                    out.print("<TABLE>");
                    out.print("<TH>"+"Match N°"+"</TH>");
                    out.print("<TH>"+"Date"+"</TH>");
                    out.print("<TH>"+"Action"+"</TH>");
                    for(Matches oMatch : listFinish){
                        out.print("<TR>");
                        out.print("<TD>"+oMatch.getiID()+"</TD>");
                        out.print("<TD>"+Dates.sdDays.format(oMatch.getoDate())+"</TD>");
                        out.print("<TD>"+"<a href=\"Controller_Game?view=finishedMatch&idMatches="+oMatch.getiID()+"\">See Match!</a>"+"</TD>");
                        out.print("</TR>");
                    }
                     out.print("</TABLE>");
                 }
            %>
        
        </article>
            
        </div>
            <jsp:include page="../footer.jsp"/>
    </body>
</html>

