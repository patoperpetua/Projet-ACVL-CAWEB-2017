<%-- 
    Document   : mainView
    Created on : 29-mar-2017, 17:59:40
    Author     : Ariel
--%>

<%@page import="java.util.Calendar"%>
<%@page import="java.util.Date"%>
<%@page import="model.Messages"%>
<%@page import="model.Matches"%>
<%@page import="Utils.Dates"%>
<%@page import="model.Chats"%>
<%@page import="model.Designations"%>
<%@page import="model.Players"%>
<%@page import="java.util.ArrayList"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ page errorPage="../WEB-INF/error.jsp" %>
<!DOCTYPE html>
<html>
    <head>
        <link rel="icon" href="Resourses/Images/favicon.png">
        <link rel="stylesheet" type="text/css" href="CSS/styles.css">
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Werewolf - The Game</title>
        <script>
            // Set the date we're counting down to
            var countDownDate = new Date();
            <%
                    Matches oMatch = (Matches) request.getSession().getAttribute("thisMatch");
                    Players oThisPlayer = (Players) request.getSession().getAttribute("thisPlayer");
                    Chats oThisChat = (Chats) request.getSession().getAttribute("thisChat");
                    ArrayList<Players> listPlayers = (ArrayList<Players>) request.getAttribute("listPlayers");
                    if (oThisPlayer != null) {
                        if (oThisPlayer.isIsWolf()) {
                            out.print("countDownDate.setHours(" + Dates.sdHours.format(oMatch.getoHourStart()) + "," + Dates.sdMinutes.format(oMatch.getoHourStart()) + ");");
                        } else {
                            out.print("countDownDate.setHours(" + Dates.sdHours.format(oMatch.getFinalDayHour()) + "," + Dates.sdMinutes.format(oMatch.getFinalDayHour()) + ");");
                        }
                    }
            %>
            if (countDownDate.getTime() > new Date().getTime()){                
                // Update the count down every 1 second
                var x = setInterval(function () {

                    // Get todays date and time
                    var now = new Date().getTime();

                    // Find the distance between now an the count down date
                    var distance = countDownDate - now;

                    // Time calculations for days, hours, minutes and seconds
                    var hours = Math.floor((distance % (1000 * 60 * 60 * 24)) / (1000 * 60 * 60));
                    var minutes = Math.floor((distance % (1000 * 60 * 60)) / (1000 * 60));
                    var seconds = Math.floor((distance % (1000 * 60)) / 1000);

                    // Display the result in the element with id="demo"
                    document.getElementById("demo").innerHTML = "Time to close chat: " + hours + "h "
                            + minutes + "m " + seconds + "s ";

                    // If the count down is finished, write some text 
                    if (distance < 0) {
                        clearInterval(x);
                        document.getElementById("demo").innerHTML = "EXPIRED";
                    }
                }, 1000);
            }
        </script>
    </head>
    <body>
        <jsp:include page="../header.jsp"/>
        <div>
        <article class="article">
        <div>
            <div>
                <%
                    int iWolfs = 0;
                    int iLivePlayers = 0;
                    int iLiveWolf = 0;
                    if (oThisPlayer != null) {
                        out.print("<h1>" + "Welcome " + oThisPlayer.getoUser().getsUsername() + "</h1>");
                        if (oThisPlayer.isIsAlived()) {
                            out.print("<h2>" + "You are ALIVE!" + "</h2>");
                        } else {
                            out.print("<h2>" + "You are DEAD!" + "</h2>");
                        }
                        if (oThisPlayer.isIsWolf()) {
                            out.print("<h2>" + "You are a WOLF" + "</h2>");
                        } else {
                            out.print("<h2>" + "You are a CITIZEN!" + "</h2>");
                        }
                        if (oThisPlayer.getsPower() != null) {
                            out.print("<h2>" + "You have the power of: " + oThisPlayer.getsPower() + "</h2>");
                        }else
                            out.print("<h2>" + "You DO NOT have any power"+ "</h2>");
                    }
                    if(oMatch != null){
                        out.print("<h2>" + "Power Probability: "+ oMatch.getfPowerProbability()+"</h2>");
                    }
                    if(listPlayers != null){
                        iWolfs = 0;
                        iLivePlayers = 0;
                        for(Players oPla : listPlayers){
                            if(oPla.isIsAlived())
                                iLivePlayers++;
                            if(oPla.isIsWolf()){
                                iWolfs++;
                                if(oPla.isIsAlived())
                                    iLiveWolf++;
                            }
                        }
                        out.print("<h2>" + "Number of wolves: "+ iWolfs+"</h2>");
                    }
                %>
            </div>

            <div class="column-left">
                <div>
                    <p>Users</p>
                    <ul>
                        <%
                            if (listPlayers != null && !listPlayers.isEmpty()) {
                                for (Players oPlayer : listPlayers) {
                                    if (!oPlayer.equals(oThisPlayer)) {
                                        out.print("<li>" + oPlayer.getoUser().getsUsername());
                                        if (oPlayer.isIsAlived()) {
                                            out.print(" - " + " ALIVE" + "</li>");
                                        } else {
                                            out.print(" - " + "DEAD" + "</li>");
                                        }
                                    }
                                }
                            }
                        %>
                    </ul>
                </div>
                <div>
                    <p>Archived Chats</p>
                    <ul>
                        <%
                            ArrayList<Chats> listChats = (ArrayList<Chats>) request.getAttribute("listChats");
                            if (listChats != null && !listChats.isEmpty()) {
                                for (Chats oChats : listChats) {
                                    out.print("<li>" + Dates.sdDays.format(oChats.getoDate()));
                                    if (oThisPlayer.isIsAlived()) {
                                        out.print("</li>");
                                    } else {
                                        out.print(" - "+"<a href=\"Controller_Game?idMatches="+oMatch.getiID()+"&view=chat&idChats="
                                                + oChats.getiID() + "\">See Chat!</a>" + "</li>");
                                    }
                                }
                            } else {
                                out.print("<p>" + "There is not previous chats!" + "</p>");
                            }
                        %>
                    </ul>
                </div>
            </div>
            <div class="column-center">Chat Section
                <div>
                    <div>
                        <%
                            Date oDate = new Date();//Date to compare if the chat is available
                            Date oBegin = new Date();//The begin time of the chat
                            Date oFinal = new Date();// The final time of the chat
                            Calendar c = Calendar.getInstance();
                            int iHours;
                            int iMinutes;
                            if (oThisPlayer.isIsWolf()) {
                                c.setTime(oMatch.getFinalDayHour());
                                iHours = c.get(Calendar.HOUR_OF_DAY);
                                iMinutes = c.get(Calendar.MINUTE);
                                c.setTime(new Date());
                                c.set(Calendar.HOUR_OF_DAY, iHours);
                                c.set(Calendar.MINUTE, iMinutes);
                                oBegin = c.getTime();
                                c.setTime(oMatch.getoHourStart());
                                iHours = c.get(Calendar.HOUR_OF_DAY);
                                iMinutes = c.get(Calendar.MINUTE);
                                c.setTime(new Date());
                                c.set(Calendar.HOUR_OF_DAY, iHours);
                                c.set(Calendar.MINUTE, iMinutes);
                                oFinal = c.getTime();
                                out.print("<p>" + "Wolf chat is available from:"
                                        + Dates.sdHoursAndMinutes.format(oMatch.getFinalDayHour())
                                        + " to " + Dates.sdHoursAndMinutes.format(oMatch.getoHourStart()) + "</p>");
                            }
                            c.setTime(oMatch.getoHourStart());
                            iHours = c.get(Calendar.HOUR_OF_DAY);
                            iMinutes = c.get(Calendar.MINUTE);
                            c.setTime(new Date());
                            c.set(Calendar.HOUR_OF_DAY, iHours);
                            c.set(Calendar.MINUTE, iMinutes);
                            oBegin = c.getTime();
                            c.setTime(oMatch.getFinalDayHour());
                            iHours = c.get(Calendar.HOUR_OF_DAY);
                            iMinutes = c.get(Calendar.MINUTE);
                            c.setTime(new Date());
                            c.add(Calendar.DAY_OF_YEAR, 1);
                            c.set(Calendar.HOUR_OF_DAY, iHours);
                            c.set(Calendar.MINUTE, iMinutes);
                            oFinal = c.getTime();
                            out.print("<p>" + "Citizen chat is available from:"
                                + Dates.sdHoursAndMinutes.format(oMatch.getoHourStart())
                                + " to " + Dates.sdHoursAndMinutes.format(oMatch.getFinalDayHour()) + "</p>");
                            boolean isAvailable = false;
                            if ((oDate.before(oFinal) && oDate.after(oBegin)) || oThisPlayer.isIsWolf())
                                isAvailable = true;
                            if(oThisChat.isIsForWolf())
                                out.print("<p>"+"Wolf Chat"+"</p>");
                            else
                                out.print("<p>"+"Citizen Chat"+"</p>");
                            if (oThisChat != null && oThisChat.getListMessages()
                                    != null && !oThisChat.getListMessages().isEmpty()) {
                                out.print("<ul>");
                                for (Messages oMessage : oThisChat.getListMessages()) {
                                    out.print("<li>");
                                    out.print(Dates.sd.format(oMessage.getoDate()) + " ");
                                    out.print(oMessage.getoPlayer().getoUser().getsUsername() + ": ");
                                    out.print(oMessage.getsMessage());
                                    out.print("</li>");
                                }
                                out.print("</ul>");
                            }else
                                out.print("<p>"+"There is no messages in this chat"+"</p>");
                            out.print("</div>");
                            if (oThisPlayer.isIsAlived() && isAvailable) {
                                out.print("<p id=\"demo\"></p>");
                                out.print("<div>");
                                out.print("<form action=\"Controller_Game\">"
                                        + "<input id=\"message\" name=\"message\" type=\"text\"/> "
                                        + "<input type=\"hidden\" name=\"idMatches\" value=\"" + oMatch.getiID() + "\" />"
                                        + "<input type=\"hidden\" name=\"action\" value=\"addMessage\"/>"
                                        + "<button>Send</button></form>");
                                out.print("</div>");
                            } else {
                                if(!isAvailable)
                                    out.print("<p>" + "As your designation group is not available, you cannot submit a mesage." + "</p>");
                                if(!oThisPlayer.isIsAlived())
                                    out.print("<p>" + "As your are not alive, you cannot submit a mesage." + "</p>");
                            }
                            
                        %>
                </div>
            </div>
            <div class="column-right">
                <div>
                    <%
                        out.print("<p>"+"Designations");
                        if(oThisChat.isIsForWolf())
                            out.print(" for WOLFS"+"</p>");
                        else
                            out.print(" for CITIZENS"+"</p>");
                        out.print("<ul>");
                        int iMissingVotes;
                        if(oThisChat.isIsForWolf())
                            iMissingVotes = iLiveWolf;
                        else
                            iMissingVotes = iLivePlayers;
                        ArrayList<Designations> listDesignations = (ArrayList<Designations>) request.getAttribute("listDesignations");
                        boolean someDesignationIsPositive = false;
                        if (listDesignations != null && !listDesignations.isEmpty()) {
                            for (Designations oDesignation : listDesignations) {
                                if(oDesignation.isPositive()){
                                    someDesignationIsPositive = true;
                                    out.print("<li>" + "Player: " + oDesignation.getoPlayerAcussed().getoUser().getsUsername()
                                        + " was killed by " + oDesignation.howManyPositivesVotes()
                                        + " positive votes  from " + oDesignation.howManyVotes() + " total votes."+"</li>");
                                }else{
                                    out.print("<li>" + "Player: " + oDesignation.getoPlayerAcusser().getoUser().getsUsername()
                                        + " designate " + oDesignation.getoPlayerAcussed().getoUser().getsUsername()
                                        + " to be killed. Votes:" + oDesignation.howManyPositivesVotes()
                                        + "/" + oDesignation.howManyVotes() + " from " + iMissingVotes + " live players.");
                                    if (isAvailable &&  oThisPlayer.isIsAlived() && !oDesignation.hasVoted(oThisPlayer) && !oDesignation.isCompleted()) {
                                        out.print("<a href=\"Controller_Game?idMatches=" + oMatch.getiID() + "&action=Vote&isPositive=yes&idDesignations=" + oDesignation.getiID() + "\">Vote Yes!</a>");
                                        out.print("  -  ");
                                        out.print("<a href=\"Controller_Game?idMatches=" + oMatch.getiID() + "&action=Vote&isPositive=no&idDesignations=" + oDesignation.getiID() + "\">Vote No!</a>");
                                    } else {
                                        if(oDesignation.hasVoted(oThisPlayer))
                                            out.print("You have voted!");
                                        if(!oThisPlayer.isIsAlived())
                                            out.print("You are not alive!");
                                        if(isAvailable)
                                            out.print("Designation not available!");
                                        if(oDesignation.isCompleted())
                                            out.print("Designation completed!");
                                    }
                                }
                                out.print("</li>");
                            }
                        } else {
                            out.print("<p>" + "There is not previous designations today!" + "</p>");
                        }
                        out.print("</ul>");
                        out.print("</div>");
                        out.print("<div id=\"DessignationForm\">");
                        out.print("<p>Designate Somebody</p>");
                        if (!isAvailable || !oThisPlayer.isIsAlived() || someDesignationIsPositive){
                            if(!isAvailable)
                                out.print("<p>" + "As your designation group is not available, you cannot designate somebody to be killed." + "</p>");
                            if(!oThisPlayer.isIsAlived())
                                out.print("<p>" + "As your are not alive, you cannot designate somebody to be killed." + "</p>");
                            if(someDesignationIsPositive){
                                out.print("<p>" + "As your a designation is positive, you cannot designate somebody to be killed." + "</p>");
                            }
                        }else{
                            out.print("<form action=\"Controller_Game\">");
                            out.print("<input type=\"hidden\" name=\"action\" value=\"addDesignation\" />");
                            out.print("<input type=\"hidden\" name=\"idMatches\" value=\""+oMatch.getiID()+"\"/>");
                            out.print("<select id=\"Accused\" name=\"Accused\">");
                            out.print("<option value=\"select\">Select</option>");
                            if (listPlayers != null && !listPlayers.isEmpty()) {
                                for (Players oPlayer : listPlayers) {
                                    if (oPlayer.isIsAlived()) {
                                        out.print("<option value=\"" + oPlayer.getoUser().getsUsername() + "\">"
                                                + oPlayer.getoUser().getsUsername() + "</option>");
                                    }
                                }
                            }
                            out.print("</select>");
                            out.print("<button>Submit</button>");
                            out.print("</form>");
                            out.print("</div>");
                            out.print("</div>");
                        }
                    %>
            </div>
        </div>
        </article>
        </div>
        <jsp:include page="../footer.jsp"/>
    </body>
</html>
