/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/* global usernames */

function checkForm(form) {
    
    for (i = 0 ; i< usernames.length ; i++){
        if(form.username.value === usernames[i]){
            alert("Error: Username is used!");
            return false;
        }
    }
    if (form.username.value === "") {
        alert("Error: Username cannot be blank!");
        form.username.focus();
        return false;
    }
    re = /^\w+$/;
    if (!re.test(form.username.value)) {
        alert("Error: Username must contain only letters, numbers and underscores!");
        form.username.focus();
        return false;
    }

    if (form.pwd1.value !== "" && form.pwd1.value === form.pwd2.value) {
        if (form.pwd1.value.length < 6) {
            alert("Error: Password must contain at least six characters!");
            form.pwd1.focus();
            return false;
        }
        if (form.pwd1.value === form.username.value) {
            alert("Error: Password must be different from Username!");
            form.pwd1.focus();
            return false;
        }
//        re = /[0-9]/;
//        if (!re.test(form.pwd1.value)) {
//            alert("Error: password must contain at least one number (0-9)!");
//            form.pwd1.focus();
//            return false;
//        }
//        re = /[a-z]/;
//        if (!re.test(form.pwd1.value)) {
//            alert("Error: password must contain at least one lowercase letter (a-z)!");
//            form.pwd1.focus();
//            return false;
//        }
//        re = /[A-Z]/;
//        if (!re.test(form.pwd1.value)) {
//            alert("Error: password must contain at least one uppercase letter (A-Z)!");
//            form.pwd1.focus();
//            return false;
//        }
    } else {
        alert("Error: Please check that you've entered and confirmed your password!");
        form.pwd1.focus();
        return false;
    }
    return true;
}

function valider() {
    valor = document.getElementById("Username").value;
    if (valor === null || valor.length === 0 || /^\s+$/.test(valor)) {
        alert("A Valid Username is required");
        return false;
    }
    valor = document.getElementById("Password").value;
    if (valor === null || valor.length === 0 || /^\s+$/.test(valor)) {
        alert("A Valid Password is required");
        return false;
    }
    return true;
}

function checkConfig(form) {
    alert("Error: N° of players cannot be blank!");
    if (form.n_player_i.value === "") {
        alert("Error: N° of players cannot be blank!");
        form.username.focus();
        return false;
    }
    var x;
    // Get the value of the input field with id="powers_probability"
    x = document.getElementById("powers_probability").value;
    // If x is Not a Number or less than 1 or greater than 100
    if (isNaN(x) || x < 1 || x > 100) {
        alert("Error: Powers probability input must be between 1 and 100");
        form.username.focus();
        return false;
    }

    // Get the value of the input field with id="werewolfsProp"
    x = document.getElementById("werewolfsProp").value;
    // If x is Not a Number or less than 1 or greater than 90
    if (isNaN(x) || x < 1 || x > 90) {
        alert("Error: Werewolfs proportion input must be between 1 and 100");
        form.username.focus();
        return false;
    }
}