<%-- 
    Document   : error
    Created on : 24/03/2017, 19:09:52
    Author     : patop
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@page isErrorPage="true" %>
<c:set var="exception" value="${requestScope['javax.servlet.error.exception']}"/>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <script type="text/javascript" src="JS/script.js"></script>
        <link rel="stylesheet" type="text/css" href="CSS/styles.css">
        <title>Projet-ACVL-CAWEB-2017 - </title>
    </head>
    <body>
        
        <jsp:include page="../header.jsp"/>
        <h1>Error Page</h1>
        <p>Servlet Name: <%= pageContext.getErrorData().getServletName()%></p>
        <p>if the servlet name is "JSP" is because error was generated in jsp file,
        not in a servlet .java</p>
        <p>Request from Page: <%= pageContext.getErrorData().getRequestURI() %></p>
        <h2>Trace Error</h2>
        <jsp:scriptlet>
            exception.printStackTrace(new java.io.PrintWriter(out));
        </jsp:scriptlet>
        <jsp:include page="../footer.jsp"/>
    </body>
</html>
